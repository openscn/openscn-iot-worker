import http, { IncomingMessage, ServerResponse } from 'http';
import { metricsHandler, error404Handler, error500Handler } from './routes';
import { ConfigService } from '../config-service/config-service';
import { Logger } from '../logger/logger';

export class HTTPService {

  private readonly server;

  constructor(
    private readonly _configService: ConfigService,
    private readonly _logger: Logger,
  ) {
    this.server = http.createServer(this.requestHandler);
  }

  private async requestHandler(req: IncomingMessage, res: ServerResponse) {
    try {
      switch(req.url) {
        case '/metrics': return await metricsHandler(req, res);
        default: return error404Handler(req, res);
      }
    } catch(err: any) {
      this._logger.error(err?.stack ?? err);
      return error500Handler(req, res);
    }
  }

  start() {
    const config = this._configService.config.http;
    this.server.listen(config.port, config.host, () => {
      this._logger.log(`Started web server at http://${config.host}:${config.port}`);
    });
  }
}