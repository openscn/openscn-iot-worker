import { IncomingMessage, ServerResponse } from "http"
import { MetricsService } from "../../metrics-service/metrics.service"

const metricsService = new MetricsService();
const headers = metricsService.getHeaders();

export const metricsHandler = async (req: IncomingMessage, res: ServerResponse) => {

    const content = await metricsService.getPrometheusMetrics();
    
    for (const [key, value] of Object.values(headers)) {
      res.setHeader(key, value);
    }

    res.writeHead(200);
    res.end(content);
}
