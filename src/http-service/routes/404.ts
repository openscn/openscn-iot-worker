import { IncomingMessage, ServerResponse } from "http";


export const error404Handler = (req: IncomingMessage, res: ServerResponse) => {
  res.writeHead(404);
  res.end('404');
}
