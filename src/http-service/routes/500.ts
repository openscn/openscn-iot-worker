import { IncomingMessage, ServerResponse } from "http";

export const error500Handler = (req: IncomingMessage, res: ServerResponse) => {
  res.writeHead(500);
  res.end('500');
}
