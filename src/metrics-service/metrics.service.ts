import * as PromClient from 'prom-client';

export class MetricsService {

  private register: PromClient.Registry<"text/plain; version=0.0.4; charset=utf-8">;

  constructor() {
    this.register = new PromClient.Registry();
    this.initialize(this.register);
  }

  private initialize(register: PromClient.Registry<"text/plain; version=0.0.4; charset=utf-8">) {
    PromClient.collectDefaultMetrics({
      prefix: 'node_',
      gcDurationBuckets: [0.001, 0.01, 0.1, 1, 2, 5],
      register: register,
    });
  }

  getHeaders() {
    return {
      'Content-Type': this.register.contentType,
    }
  }

  async getPrometheusMetrics() {
    return await this.register.metrics();
  }
}