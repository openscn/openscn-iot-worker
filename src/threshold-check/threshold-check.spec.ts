import { ThresholdCheckProcessor } from "./threshold-check";
import { before, describe } from "node:test";
import { ConfigService } from "../config-service/config-service";
import { InfluxQueryService } from "../influx-query-service/influx-query-service";
import { Logger } from "../logger/logger";

describe("hello", () => {

  let service: ThresholdCheckProcessor;

  beforeEach(() => {
    const configService = new ConfigService();
    const influx = new InfluxQueryService({
      org: "",
      bucket: "",
      token: "",
      host: ""
    }, new Logger(""), true);
    service = new ThresholdCheckProcessor(configService, influx, new Logger(''));
  });

  it("should create", () => {
    expect(service).toBeDefined();
  });

  describe("resolveStatus method", () => {
    it("should resolve normal", () => {
      const result1 = service.resolveStatus({
        number: 10,
        criticalThreshold: 12,
        warningThreshold: 11,
        operator: "gt"
      });

      const result2 = service.resolveStatus({
        number: 10,
        criticalThreshold: 8,
        warningThreshold: 9,
        operator: "lt"
      });

      expect(result1).toBe("normal");
      expect(result2).toBe("normal");
    });

    it("should resolve warning", () => {
      const result1 = service.resolveStatus({
        number: 12,
        criticalThreshold: 12,
        warningThreshold: 11,
        operator: "gt"
      });

      const result2 = service.resolveStatus({
        number: 8,
        criticalThreshold: 8,
        warningThreshold: 9,
        operator: "lt"
      });

      expect(result1).toBe("warning");
      expect(result2).toBe("warning");
    });

    it("should resolve critical", () => {
      const result1 = service.resolveStatus({
        number: 13,
        criticalThreshold: 12,
        warningThreshold: 11,
        operator: "gt"
      });

      const result2 = service.resolveStatus({
        number: 7,
        criticalThreshold: 8,
        warningThreshold: 9,
        operator: "lt"
      });

      expect(result1).toBe("critical");
      expect(result2).toBe("critical");
    });
  });
});
