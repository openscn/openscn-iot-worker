export class Logger {
  private readonly prefix: string;

  constructor(context: string) {
    this.prefix = `[${context}]`;
  }

  log(...args: unknown[]) {
    const date = this.getTime();
    console.log(this.prefix, date, '-', ...args);
  }

  debug(...args: unknown[]) {
    const date = this.getTime();
    console.debug(this.prefix, date, '-', ...args);
  }

  verbose(...args: unknown[]) {
    const date = this.getTime();
    console.info(this.prefix, date, '-', ...args);
  }

  error(...args: unknown[]) {
    const date = this.getTime();
    console.error(this.prefix, date, '-', ...args);
  }

  getTime() {
    const d = new Date();
    const month = ("00" + (d.getMonth() + 1)).slice(-2);
    const seconds = ("00" + d.getSeconds()).slice(-2);
    const date = `${d.getFullYear()}-${month}-${d.getDate()}`;

    const time = `${d.getHours()}:${d.getMinutes()}:${seconds}`;
    return `${date} ${time}`;
  }
}
