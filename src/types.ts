export interface ProcessResult {
  status: 'error' | 'success' | 'undefined';
  shouldReport: boolean;
  data?: unknown;
}
